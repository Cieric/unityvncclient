using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VNCClient : MonoBehaviour
{
    [SerializeField]
    Texture2D texture;

    [SerializeField]
    RawImage rawImage;

    UnityVNC.Rectangle rect;

    UnityVNC.VNCClient client;

    // Start is called before the first frame update
    void Start()
    {
        rawImage = GetComponent<RawImage>();
        StartCoroutine(StartVNC());
    }

    private IEnumerator StartVNC()
    {
        client = new UnityVNC.VNCClient("127.0.0.1", 5900);
        client.Start();
        while(!client.IsRunning) yield return null;
        texture = new Texture2D(1920, 1080, TextureFormat.RGBA32, false);
        texture.Apply();
        rawImage.texture = texture;
        rect = new UnityVNC.Rectangle(0, 0, client.frameBuffer.Width, client.frameBuffer.Height);
    }

    // Update is called once per frame
    void Update()
    {
        if (client != null && client.IsRunning)
        {
            if(Input.GetKeyDown(KeyCode.Alpha7))
			{
                client.FrameBufferUpdateRequest(rect);
                Debug.Log("Getting Frame!");
            }
            if (texture != null && client?.frameBuffer?.Screen != null)
            {
                var data = texture.GetRawTextureData<Color32>();
                // fill texture data with a simple pattern
                int bbp = client.frameBuffer.PixelFormat.bitsPerPixel / 8;
                int index = 0;
                for (int y = 0; y < texture.height; y++)
                {
                    for (int x = 0; x < texture.width; x++)
                    {
                        byte r = client.frameBuffer.Screen[(x + y * client.frameBuffer.Width) * bbp + 0];
                        byte g = client.frameBuffer.Screen[(x + y * client.frameBuffer.Width) * bbp + 1];
                        byte b = client.frameBuffer.Screen[(x + y * client.frameBuffer.Width) * bbp + 2];
                        byte a = 255;
                        data[index++] = new Color32(r, g, b, a);
                    }
                }
                // upload to the GPU
                texture.Apply();
            }
        }
    }
}
